package service;


import CommonMethods.CommonMethods;
import com.cucumber.listener.Reporter;
import dto.UsuariosDTO;
import dto.baseDTO.SimuladorDTOBase;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.json.JSONArray;



import static io.restassured.RestAssured.get;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.Assert;


public class RequisicaoAPI {


	private String urlSimulacoes = "http://localhost:8080/api/v1/";
	private String endpointRestricao ="restricoes/";
	private String endpointSimulacao ="simulacoes/";
	 String respostaId;
	 String respostaNome;
      CommonMethods cm = new CommonMethods();



	public void putUserTest(String nome,String cpf){

		SimuladorDTOBase baseDto = new SimuladorDTOBase();
		UsuariosDTO dados =baseDto.getSimuladorBaseRealizado();
		JSONObject requestParams = new JSONObject();
		RequestSpecification request = given();
		dados.setNome("André Marinho");
		requestParams.put("nome", nome);
		requestParams.put("cpf", cpf);
		requestParams.put("email", dados.email);
		requestParams.put("valor", dados.valor);
		requestParams.put("parcelas", dados.parcelas);
		requestParams.put("seguro", dados.seguro);
		request.header("Content-Type", "application/json");
		request.body(requestParams.toString());
		Response response  = request.put(urlSimulacoes+endpointSimulacao+cpf);
		response.getStatusCode();
		Assert.assertEquals(200, response.getStatusCode());
		Reporter.addStepLog(response.prettyPrint());

	}
	public String  valida_endpoint_restricao(){
		String responseString =get(urlSimulacoes).
				then().
				statusCode(200).
				extract().
				path("mensagem");

		System.out.println(urlSimulacoes );
		return responseString;
	}
	public void validaRestricaoCpf(String mensagem){
		if(mensagem.contains("tem problema")){
			Reporter.addStepLog("CPF com restrição");
		}else{
			Reporter.addStepLog("CPF não encontrado");
		}
	}
	public void validaSimulacaoCpf(String mensagem){
		if(mensagem.contains("não encontrado")){
			Reporter.addStepLog("CPF não encontrado");
		}else{
			Reporter.addStepLog("CPF encontrado");
		}
	}
	public String getRestricaoByCPF (String cpf) {
		RestAssured.baseURI =urlSimulacoes;

			Reporter.addStepLog("Reponse:" +urlSimulacoes+endpointRestricao+cpf);
			System.out.println(urlSimulacoes+endpointRestricao+cpf);
			Response response = given()
				.contentType(ContentType.JSON)
				.when()
				.get(endpointRestricao+cpf)
				.then()
				.extract().response();
			if(response.contentType().isEmpty()){
				Assert.assertEquals(204,response.statusCode());
				return "não encontrado" ;
			   }else

			{
				Assert.assertEquals(200,response.statusCode());
				return response.path("mensagem");
			}
	}
	public String getCPFSimulacao(){
	String path =urlSimulacoes+endpointSimulacao;
	Reporter.addStepLog("Reponse:" +path);
	JsonPath retorno = given()
			.header("Accept", "application/json")
			.get(path)
			.andReturn().jsonPath();

	List<String> UsuariosDTO = retorno.getJsonObject("cpf");
	String cpf =UsuariosDTO.get(0).toString();
	return cpf;
}
	public Integer getIdSimulacao(){
		List<String> simulacoes =getListSimulacao();
		int registros =simulacoes.size();
		String path =urlSimulacoes+endpointSimulacao;
		Reporter.addStepLog("Reponse:" +path);
		JsonPath retorno = given()
				.header("Accept", "application/json")
				.get(path)
				.andReturn().jsonPath();

		List<Integer> UsuariosDTO = retorno.getJsonObject("id");
		Integer id =UsuariosDTO.get(registros-1);
		Reporter.addStepLog("id encontrado na simulação : "+id);
		return id;
	}
	public String getSimulacaoByCPF(String cpf) {

		RestAssured.baseURI = urlSimulacoes;
		Reporter.addStepLog("Reponse:" + urlSimulacoes + endpointSimulacao + cpf);
		Response response = given()
				.contentType(ContentType.JSON)
				.when()
				.get(endpointSimulacao + cpf)
				.then()
				.extract().response();
		String mensagem = response.path("mensagem");
		String resp="";
		if (mensagem == null) {
			Assert.assertEquals(200, response.statusCode());


		} else if (mensagem.contains("não encontrado")) {
			Assert.assertEquals(404, response.statusCode());
			resp= response.path("mensagem");
		}
         return resp;

	}
	public List<String> getListSimulacao(){
		String path =urlSimulacoes+endpointSimulacao;
		JsonPath retorno = given()
				.header("Accept", "application/json")
				.get(path)
				.andReturn().jsonPath();
				List<String> UsuariosDTO = retorno.getJsonObject("cpf");
		return UsuariosDTO;
	}
	public void realiza_uma_simulacao_com_cpf_existente(String cpf){
		RestAssured.baseURI = "http://localhost:8080/api/v1/";
		UsuariosDTO dados = new UsuariosDTO();
		SimuladorDTOBase baseDto = new SimuladorDTOBase();
		dados = baseDto.getSimuladorBaseRealizado();
		dados.cpf =cpf;

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(dados)
				.when()
				.post("simulacoes")
				.then()
				.extract().response();
		Reporter.addStepLog("Reponse:" +response.path("mensagem"));
		Assert.assertEquals(400, response.getStatusCode());

	}
	public void postSimualacaoCpfIncorreto(String cpf){
		RestAssured.baseURI = "http://localhost:8080/api/v1/";
		UsuariosDTO dados = new UsuariosDTO();
		SimuladorDTOBase baseDto = new SimuladorDTOBase();
		dados = baseDto.getSimuladorBaseRealizado();
		dados.cpf =cpf;

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(dados)
				.when()
				.post(endpointSimulacao)
				.then()
				.extract().response();
		Reporter.addStepLog("Reponse:" +response.path("mensagem"));
		Assert.assertEquals(201, response.getStatusCode());

	}
	public void postSimualacaoDadosValidos(){
		RestAssured.baseURI = "http://localhost:8080/api/v1/";
		SimuladorDTOBase baseDto = new SimuladorDTOBase();
		UsuariosDTO dados = baseDto.getSimuladorBaseRealizado();
		JsonPath retorno = given()
						.header("Accept", "application/json")
						.contentType("application/json")
						.body(dados)
						.expect()
						.statusCode(201)
						.when()
						.post(endpointSimulacao)
						.andReturn()
						.jsonPath();
		Reporter.addStepLog("Reponse:" +retorno.prettyPrint());
		String resposta =  retorno.getJsonObject("nome");
		assertEquals(dados.nome, resposta);



	}
	public int  getQuantidadeSimulacoes(List<String> simulacoes){
		int registros=0;
		if(!simulacoes.isEmpty()) {
			registros = simulacoes.size();
			for (String simulacao:simulacoes) {
				Reporter.addStepLog("cpf : "+simulacao);
			}
			Reporter.addStepLog("Registros encontrados :" +simulacoes.size());
		}else{
			Reporter.addStepLog("Não foram encontrados simulações");
			Assert.fail("Não foram encontrados simulações");
		}

		return registros;

	}
	public void validaUltmimaSimulacao(){
		List<String> simulacoes =getListSimulacao();
		int registros =simulacoes.size();
		String path =urlSimulacoes+endpointSimulacao;
		Reporter.addStepLog("Reponse:" +path);
		JsonPath retorno = given()
				.header("Accept", "application/json")
				.get(path)
				.andReturn().jsonPath();

		List<String> UsuariosDTO = retorno.getJsonObject("cpf");
		String cpf =UsuariosDTO.get(registros-1).toString();
		Reporter.addStepLog("CPF encontrado na simulação : "+cpf);
	}
	public void validaSimulacaoByCPF(String cpf){
		List<String> simulacoes =getListSimulacao();
		int registros =simulacoes.size();
		String path =urlSimulacoes+endpointSimulacao+cpf;
		Reporter.addStepLog("Reponse:" +path);
		JsonPath retorno = given()
				.header("Accept", "application/json")
				.expect()
				.statusCode(200)
				.when()
				.get(path)
				.andReturn().jsonPath();
		String UsuariosDTO = retorno.getJsonObject("cpf");
		String cpfs =UsuariosDTO;
		Reporter.addStepLog("CPF encontrado na simulação : "+cpfs);
	}
	public void deleteUserTest(Integer id){
		Response response = given()
				.header("Content-type", "application/json")
				.when()
				.delete(urlSimulacoes+endpointSimulacao+id)
				.then()
				.extract().response();
			Assert.assertEquals(200, response.getStatusCode());
			Reporter.addStepLog(urlSimulacoes+endpointSimulacao+id);

	}
	public void confirmIdDeletado(Integer id){
		Reporter.addStepLog(urlSimulacoes+endpointSimulacao+"?id="+id);
		when().
				get(urlSimulacoes+endpointSimulacao+"?id="+id).
				then().
				statusCode(200);

	}
	public void confirmCpfUpdated(String nome, String cpf){
		String path =urlSimulacoes+endpointSimulacao+cpf;
		Reporter.addStepLog("Reponse:" +path);
		JsonPath retorno = given()
				.header("Accept", "application/json")
				.expect()
				.statusCode(200).and()
				.body("nome",Matchers.is(nome))
				.when()
				.get(path)
				.andReturn().jsonPath();
		String UsuariosDTO = retorno.getJsonObject("nome");
		  nome =UsuariosDTO;
		  Reporter.addStepLog("Nome atualizado para : "+nome);

	}
	public void consultarRestricao(){
		String path ="src/test/resources/Arquivos/restricao.json";
		String arquivo =cm.lerArquivo(path);
		JSONArray array = new JSONArray(arquivo);
		int i = 0;
		String cpf =array.getString(0);

		while(i < array.length()){
			cpf =array.getString(i);
			String urli =urlSimulacoes+endpointRestricao+cpf;
			System.out.println(urlSimulacoes+endpointRestricao+cpf);
			Reporter.addStepLog("Reponse:" +urli);
			String responseString =get(urli).
					then().
					statusCode(200).
					extract().
					path("mensagem");

			Assert.assertNotNull(responseString);
			i++;
		}

	}
	public String consultaCPFPUT(String cpf){
		RestAssured.baseURI = urlSimulacoes;
		Reporter.addStepLog("Reponse:" + urlSimulacoes + endpointSimulacao + cpf);
		Response response = given()
				.contentType(ContentType.JSON)
				.when()
				.get(endpointSimulacao + cpf)
				.then()
				.extract().response();
		String mensagem = response.path("mensagem");
		String resp="";
		if (mensagem == null) {
			Assert.assertEquals(200, response.statusCode());
			resp =cpf;

		} else if (mensagem.contains("não encontrado")) {
			Assert.assertEquals(404, response.statusCode());
			resp= response.path("mensagem");
		}
		return resp;
	}



	}





	
		


