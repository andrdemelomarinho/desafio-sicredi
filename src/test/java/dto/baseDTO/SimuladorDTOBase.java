package dto.baseDTO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

import dto.UsuariosDTO;
import org.apache.commons.lang.RandomStringUtils;

//import org.apache.commons.lang3.RandomStringUtils;

import dto.SimuladorDTO;


public class SimuladorDTOBase {

	public  UsuariosDTO getSimuladorBaseRealizado() {
		int getRandomValue = ThreadLocalRandom.current().nextInt(1111111, 9999999);
		UsuariosDTO simulador = new UsuariosDTO();
		simulador.setCpf("0078"+getRandomValue);
		simulador.setNome("Andre Marinho");
		simulador.setEmail("andre@teste.com.br");
		simulador.setValor(2000);
		simulador.setParcela(24);
		simulador.setSeguro(true);
		return simulador;
	}
	
}
