package dto.baseDTO;

import dto.CustomerDTO;

import java.util.concurrent.ThreadLocalRandom;

public class CustomerDTOBase {

	public  CustomerDTO getCustomerBaseRealizado() {
		CustomerDTO customer = new CustomerDTO();
		customer.setCustomerName("Teste Sicredi");
		customer.setContactLastName("Teste");
		customer.setContactFirstName("Andre");
		customer.setPhone("51 9999-9999");
		customer.setAddressLine1("Av Assis Brasil, 3970");
		customer.setAddressLine2("Torre D");
		customer.setCity("Porto Alegre");
		customer.setState("RS");
		customer.setPostalCode("91000-000");
		customer.setCountry("Brasil");
		customer.setSalesRepEmployeeNumber("123456");
		customer.setCreditLimit("200");

		return customer;
	}
	
}
