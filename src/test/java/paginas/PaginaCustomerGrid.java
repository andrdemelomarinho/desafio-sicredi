package paginas;
import com.cucumber.listener.Reporter;
import dto.CustomerDTO;
import dto.UsuariosDTO;
import dto.baseDTO.CustomerDTOBase;
import dto.baseDTO.SimuladorDTOBase;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import session.ThreadManager;

import org.openqa.selenium.WebDriver;

public class PaginaCustomerGrid {
    public PaginaCustomerGrid (WebDriver driver) {this.getDriver();}
    public WebDriver getDriver() {
        return ThreadManager.getSession().getDriver();
    }
   CustomerDTOBase dtoBase = new CustomerDTOBase();
    CustomerDTO dados =dtoBase.getCustomerBaseRealizado();
    public void preencherFormulario(){
        customerNameForm(dados.customerName);
        contactLastNameForm(dados.contactLastName);
        contactFirstNameForm(dados.contactFirstName);
        contactPhoneForm(dados.phone);
        addressLine1Form(dados.addressLine1);
        addressLine2Form(dados.addressLine2);
        cityForm(dados.city);
        stateForm(dados.state);
        postalCodeForm(dados.postalCode);
        countryForm(dados.country);
        salesRepEmployeeNumberForm(dados.salesRepEmployeeNumber);
        creditLimitForm(dados.creditLimit);

    }
    public void clickAddRecordButton(){
        WebElement addRecord =getDriver().findElement(By.xpath("//div[@class='header-tools']/div/a[@class='btn btn-default btn-outline-dark']"));
        addRecord.click();
    }
    public void selectTheme(String text){
        WebElement selectElement =getDriver().findElement(By.id("switch-version-select"));
        Select selectObject = new Select(selectElement);
        selectObject.selectByVisibleText(text);
    }
    public void customerNameForm(String nome){
        WebElement name =getDriver().findElement(By.id("field-customerName"));
        name.sendKeys(nome);
    }
    public void contactLastNameForm(String nome){
        WebElement lastName =getDriver().findElement(By.id("field-contactLastName"));
        lastName.sendKeys(nome);
    }
    public void contactFirstNameForm(String nome){
        WebElement lastName =getDriver().findElement(By.id("field-contactFirstName"));
        lastName.sendKeys(nome);
    }
    public void contactPhoneForm(String phone){
        WebElement fone =getDriver().findElement(By.id("field-phone"));
        fone.sendKeys(phone);
    }
    public void addressLine1Form(String address){
        WebElement endereco1 =getDriver().findElement(By.id("field-addressLine1"));
        endereco1.sendKeys(address);
    }
    public void addressLine2Form(String address2){
        WebElement endereco2 =getDriver().findElement(By.id("field-addressLine2"));
        endereco2.sendKeys(address2);
    }
    public void cityForm(String city){
        WebElement cidade =getDriver().findElement(By.id("field-city"));
        cidade.sendKeys(city);
    }
    public void stateForm(String state){
        WebElement estado =getDriver().findElement(By.id("field-state"));
        estado.sendKeys(state);
    }
    public void postalCodeForm(String zip){
        WebElement cep =getDriver().findElement(By.id("field-postalCode"));
        cep.sendKeys(zip);
    }
    public void countryForm(String country){
        WebElement cidade =getDriver().findElement(By.id("field-country"));
        cidade.sendKeys(country);
    }
    public void salesRepEmployeeNumberForm(String employee){
        WebElement empregado =getDriver().findElement(By.id("field-salesRepEmployeeNumber"));
        empregado.sendKeys(employee);
    }
    public void creditLimitForm(String credit){
        WebElement credito =getDriver().findElement(By.id("field-creditLimit"));
        credito.sendKeys(credit);
    }
    public void saveButtonForm(){
        WebElement save =getDriver().findElement(By.id("form-button-save"));
        save.click();
    }
    public void validaMensagemSucessoForm() throws InterruptedException {
        Thread.sleep(2000);
        String messasge =getDriver().findElement(By.id("report-success")).getText();
        Reporter.addStepLog("Reponse: " +messasge);
        Assert.assertEquals(messasge,"Your data has been successfully stored into the database. Edit Record or Go back to list");

    }


}
