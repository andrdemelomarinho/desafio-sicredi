package stepDefinition;

import CommonMethods.CommonMethods;
import automation.Pages;
import automation.ProjectBase;
import automation.utils.ArquivoUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import paginas.PaginaCustomerGrid;
import paginas.PaginaPesquisa;
import session.ThreadManager;

public class FormularioCustomerStepDefinition extends ProjectBase {



    private Pages getPages() {
        return ThreadManager.getSession().getPages();
    }
    private WebDriver driver;
    CommonMethods cm = new CommonMethods();


    @When("^altera campo version para \"([^\"]*)\"$")
    public void altera_campo_version_para(String texto) throws Throwable {
        getPages().get(PaginaCustomerGrid.class).selectTheme(texto);
    }

    @When("^Clica no botão Add Customer$")
    public void clica_no_botão_Add_Customer() throws Throwable {
        getPages().get(PaginaCustomerGrid.class).clickAddRecordButton();

    }

    @When("^Preencher os campos do formulário$")
    public void preencher_os_campos_do_formulário() throws Throwable {
        getPages().get(PaginaCustomerGrid.class).preencherFormulario();

    }

    @When("^Cica no botão Save$")
    public void cica_no_botão_Save() throws Throwable {
        getPages().get(PaginaCustomerGrid.class).saveButtonForm();

    }

    @Then("^Validar a mensagem de sucesso$")
    public void validar_a_mensagem_de_sucesso() throws Throwable {
        getPages().get(PaginaCustomerGrid.class).validaMensagemSucessoForm();

    }
}
